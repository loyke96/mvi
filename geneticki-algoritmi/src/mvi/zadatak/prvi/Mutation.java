package mvi.zadatak.prvi;

import java.util.Random;

public class Mutation {
	static private Random random = new Random();

	// mutiranje jednog gena tako da se jedan bit promjeni
	static public int mutate(int gen) {
		int mutationBit = 1 << random.nextInt(31);
		return gen ^ mutationBit;
	}
}
