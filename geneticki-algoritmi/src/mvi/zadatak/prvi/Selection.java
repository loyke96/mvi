package mvi.zadatak.prvi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Selection {

	// turnirska selekcija jedinki za sledecu iteraciju
	static public ArrayList<Globe> select(List<Globe> population, int k) {
		ArrayList<Globe> newPopulation = new ArrayList<>();
		ArrayList<Globe> tempPopulation = new ArrayList<>();

		int iterations = population.size() / 2;
		for (int i = 0; i < iterations; i++) {
			tempPopulation.clear();

			Collections.shuffle(population);
			for (int j = 0; j < k; j++) {
				tempPopulation.add(population.get(j));
			}

			Globe best = tempPopulation.remove(0);
			while (!tempPopulation.isEmpty()) {
				Globe next = tempPopulation.remove(0);
				if (next.getPenalty() < best.getPenalty()) {
					best = next;
				}
			}

			newPopulation.add(best);
		}
		return newPopulation;
	}
}
