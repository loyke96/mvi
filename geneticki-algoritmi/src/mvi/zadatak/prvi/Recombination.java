package mvi.zadatak.prvi;

public class Recombination {

	// rekombinacija 2 gena zamjenom bita na parnim pozicijama
	static public int[] recombine(int gen1, int gen2) {
		int tmp1 = gen1 & 0x55555555;
		int tmp2 = gen2 & 0x55555555;

		gen1 = (gen1 & 0xaaaaaaaa) | tmp2;
		gen2 = (gen2 & 0xaaaaaaaa) | tmp1;

		return new int[] { gen1, gen2 };
	}
}
