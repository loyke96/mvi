package mvi.zadatak.prvi;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class Simulator {
	static private int populationSize = 64;
	static private int k = 4;

	// pocetak simulacije
	static public ArrayList<Globe> start() {
		ArrayList<Globe> population = Population.generate(populationSize);
		ArrayList<Globe> foundedUnits;

		int counter = 1;
		while (true) {
			// ukoliko postoji jedinka koja odgovara zahtjevu simulacija se zavrsava
			foundedUnits = population.stream().filter(globe -> globe.getPenalty() == 0)
					.collect(Collectors.toCollection(ArrayList::new));
			if (!foundedUnits.isEmpty()) {
				break;
			}

			counter++;
			ArrayList<Globe> selected = Selection.select(population, k);
			population.clear();
			while (!selected.isEmpty()) {

				Globe parent1 = selected.remove(0);
				Globe parent2 = selected.remove(0);

				int[] speeds = Recombination.recombine(parent1.getSpeed(), parent2.getSpeed());
				int[] angles = Recombination.recombine(parent1.getAngle(), parent2.getAngle());

				speeds[0] = Mutation.mutate(speeds[0]);
				speeds[1] = Mutation.mutate(speeds[1]);
				angles[0] = Mutation.mutate(angles[0]);
				angles[1] = Mutation.mutate(angles[1]);

				population.add(new Globe(speeds[0], angles[0]));
				population.add(new Globe(speeds[0], angles[1]));
				population.add(new Globe(speeds[1], angles[0]));
				population.add(new Globe(speeds[1], angles[1]));
			}
		}
		System.out.println("Finnished");
		System.out.println("Broj iteracija: " + counter);
		return foundedUnits;
	}
}
