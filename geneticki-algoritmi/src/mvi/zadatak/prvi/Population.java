package mvi.zadatak.prvi;

import java.util.ArrayList;
import java.util.Random;

public class Population {
	static private Random random = new Random();

	// generisanje populacije zadate velicine
	static public ArrayList<Globe> generate(int size) {
		ArrayList<Globe> population = new ArrayList<>();
		while (size-- > 0) {
			population.add(new Globe(random.nextInt(Integer.MAX_VALUE), random.nextInt(Integer.MAX_VALUE)));
		}
		return population;
	}
}
