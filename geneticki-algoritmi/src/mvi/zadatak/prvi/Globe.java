package mvi.zadatak.prvi;

public class Globe {
	private int speed; // brzina kodovana binarno koristeci 32 bita
	private int angle; // ugao kodovan binarno koristeci 32 bita
	private double penalty; // pokazatelj odstupanja od trazenih vrijednosti

	public Globe(int speed, int angle) {
		this.speed = speed;
		this.angle = angle;
		this.penalty = Physics.calculatePenalty(speed, angle);
	}

	public double getPenalty() {
		return penalty;
	}

	public int getAngle() {
		return angle;
	}

	public int getSpeed() {
		return speed;
	}
}
