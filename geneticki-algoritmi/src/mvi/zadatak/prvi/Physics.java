package mvi.zadatak.prvi;

public class Physics {
	static public double globeBarrierDistance;
	static public double barrierHoleDistance;
	static public double globeHoleDistance;

	static private final double GRAVITY = 9.81;

	// racuna odstupanje od trazenih vrijednosti
	static public double calculatePenalty(double speed, double angle) {
		if (speed == 0) {
			return Double.MAX_VALUE;
		}
		double penalty = 0.0;

		// racunanje stvarnih vrijednosti iz vrijednosti koje su binarno kodovane
		speed = (speed / Integer.MAX_VALUE) * 22;
		angle = (angle / Integer.MAX_VALUE) * 1.5;

		// racunanje dometa i visine u trenutku prolazka kroz prepreku
		double range = speed * speed * Math.sin(2 * angle) / GRAVITY;
		double barrierHeightHit = (globeBarrierDistance * Math.tan(angle))
				- ((GRAVITY / 2) * Math.pow(globeBarrierDistance / (speed * Math.cos(angle)), 2));

		// dodavanje penala proporcionalno udaljenosti od rupe
		if (range < globeHoleDistance + 0.3) {
			penalty += (globeHoleDistance + 0.3 - range);
		} else if (range > globeHoleDistance + 3.7) {
			penalty += (range - globeHoleDistance - 3.7);
		}

		// dodavanje penala proporcionalno udaljenosti od otvora na barijeri
		if (barrierHeightHit < 6.3) {
			penalty += ((6.3 - barrierHeightHit) * 2.5);
		} else if (barrierHeightHit > 9.7) {
			penalty += ((barrierHeightHit - 9.7) * 2.5);
		}

		return penalty;
	}
}
