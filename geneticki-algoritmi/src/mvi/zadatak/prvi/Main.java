package mvi.zadatak.prvi;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Udaljenost prepreke od kugle: ");
        Physics.globeBarrierDistance = scanner.nextDouble();
        System.out.print("Udaljenost rupe od prepreke: ");
        Physics.barrierHoleDistance = scanner.nextDouble();
        scanner.close();
        Physics.globeHoleDistance = Physics.globeBarrierDistance + Physics.barrierHoleDistance;

        Instant start = Instant.now();
        ArrayList<Globe> pairs = Simulator.start();
        Instant finnish = Instant.now();
        Duration duration = Duration.between(start, finnish);
        for (Globe pair : pairs) {
            double realSpeed = (pair.getSpeed() / (double) Integer.MAX_VALUE) * 22;
            double realAngle = (pair.getAngle() / (double) Integer.MAX_VALUE) * 1.5;
            realAngle = Math.toDegrees(realAngle);
            System.out.println(realSpeed + " -> " + realAngle);
        }
        Runtime runtime = Runtime.getRuntime();
        long usedMemory = runtime.totalMemory() - runtime.freeMemory();
        System.out.println("Vrijeme izvrsavanja: " + duration.toMillis() + " ms");
        System.out.println("Zauzeće memorije: " + usedMemory / 1000 + " kB");
    }
}
