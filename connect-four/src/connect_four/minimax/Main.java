package connect_four.minimax;

import java.time.Duration;
import java.time.Instant;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		char[][] state = { { ' ', ' ', ' ', ' ', ' ', ' ', ' ' }, { ' ', ' ', ' ', ' ', ' ', ' ', ' ' },
				{ ' ', ' ', ' ', ' ', ' ', ' ', ' ' }, { ' ', ' ', ' ', ' ', ' ', ' ', ' ' },
				{ ' ', ' ', ' ', ' ', ' ', ' ', ' ' }, { ' ', ' ', ' ', 'X', ' ', ' ', ' ' } };
		TableWriter.write(state);
		while (true) {
			System.out.println("Your turn (1 - 7)");
			int turn = scanner.nextInt();
			if (Children.getForInput(state, turn) == true) {
				TableWriter.write(state);
				if (Heuristic.calculate(state) == Double.NEGATIVE_INFINITY) {
					System.out.println("You won!");
					break;
				}
				System.out.println("AI is thinking...");
				Instant start = Instant.now();
				state = MinimaxDecision.nextMove(state);
				Instant end = Instant.now();
				Duration duration = Duration.between(start, end);
				System.out.println("Move calculated in: " + duration.toMillis() + " ms");
				if (state == null) {
					System.out.println("Draw.");
					break;
				}
				TableWriter.write(state);
				if (Heuristic.calculate(state) == Double.POSITIVE_INFINITY) {
					System.out.println("AI won!");
					break;
				}
			} else {
				System.out.println("column is fully occupied. Try again!");
			}
		}
		scanner.close();
	}
}
