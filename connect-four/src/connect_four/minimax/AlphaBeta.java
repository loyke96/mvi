package connect_four.minimax;

import java.util.ArrayList;

/**
 * @author Željko Lojić
 *
 */
public class AlphaBeta {

	/**
	 * @param state     stanje na platformi predstavljeno u formi matrice
	 * @param depth     trenutna dubina stabla minimax algoritma pretraživanja
	 * @param alpha     vrijednost najboljeg izbora dosad pronađenog na putanji za
	 *                  MAX
	 * @param beta      vrijednost najboljeg izbora dosad pronađenog na putanji za
	 *                  MIN
	 * @param maxPlayer vrijednost koja govori da li se traži MAX ili MIN
	 * @return Korisnost poteza, tj. ocjenu stanja
	 */
	public static double prunning(char[][] state, int depth, double alpha, double beta, boolean maxPlayer) {
		double utility = Heuristic.calculate(state);
		if (depth == 0 || utility == Double.NEGATIVE_INFINITY || utility == Double.POSITIVE_INFINITY) {
			return utility;
		}
		if (maxPlayer) {
			ArrayList<char[][]> children = Children.getAll(state, 'X');
			if (children.isEmpty()) {
				return utility;
			}
			utility = Double.NEGATIVE_INFINITY;
			for (char[][] cs : children) {
				utility = Math.max(utility, prunning(cs, depth - 1, alpha, beta, false));
				alpha = Math.max(alpha, utility);
				if (alpha >= beta) {
					break;
				}
			}
		} else {
			ArrayList<char[][]> children = Children.getAll(state, 'O');
			if (children.isEmpty()) {
				return utility;
			}
			utility = Double.POSITIVE_INFINITY;
			for (char[][] cs : children) {
				utility = Math.min(utility, prunning(cs, depth - 1, alpha, beta, true));
				beta = Math.min(beta, utility);
				if (alpha >= beta) {
					break;
				}
			}
		}
		return utility;
	}
}
