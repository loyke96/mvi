package connect_four.minimax;

import java.util.ArrayList;

/**
 * @author Željko Lojić
 *
 */
public class MinimaxDecision {
	/**
	 * @param state stanje na platformi predstavljeno u formi matrice
	 * @return Najbolji pronađen potez
	 */
	public static char[][] nextMove(char[][] state) {
		ArrayList<char[][]> candidates = Children.getAll(state, 'X');
		double bestUtil = Double.NEGATIVE_INFINITY;
		char[][] bestMove = null;
		for (char[][] cs : candidates) {
			double util = AlphaBeta.prunning(cs, 9, Double.NEGATIVE_INFINITY, Double.POSITIVE_INFINITY, false);
			if (util >= bestUtil) {
				bestMove = cs;
				bestUtil = util;
			}
		}
		return bestMove;
	}
}
