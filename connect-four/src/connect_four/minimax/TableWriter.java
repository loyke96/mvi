package connect_four.minimax;

/**
 * @author Željko Lojić
 *
 */
public class TableWriter {
	/**
	 * @param state stanje na platformi koje se ispisuje na konzolu predstavljeno u
	 *              formi matrice
	 */
	public static void write(char[][] state) {
		for (int i = 0; i < state.length; i++) {
			System.out.print('|');
			for (int j = 0; j < state[i].length; j++) {
				System.out.print(state[i][j]);
				System.out.print('|');
			}
			System.out.println();
			System.out.println("|-|-|-|-|-|-|-|");
		}
	}
}
