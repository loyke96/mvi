package connect_four.minimax;

import java.util.ArrayList;

/**
 * @author Željko Lojić
 *
 */
public class Children {
	/**
	 * @param state      stanje na platformi predstavljeno u formi matrice
	 * @param playerMark simbol koji je korišten od strane igrača
	 * @return Svi potezi koje igrač može da odigra
	 */
	public static ArrayList<char[][]> getAll(char[][] state, char playerMark) {
		ArrayList<char[][]> children = new ArrayList<>();
		for (int i = 0; i < state[0].length; i++) {
			if (state[0][i] == ' ') {
				for (int j = 1; j <= state.length; j++) {
					if (j == state.length || state[j][i] != ' ') {
						char[][] clone = new char[state.length][];
						for (int k = 0; k < state.length; k++) {
							clone[k] = state[k].clone();
						}
						clone[j - 1][i] = playerMark;
						children.add(clone);
						break;
					}
				}
			}
		}
		return children;
	}

	/**
	 * @param state stanje na platformi predstavljeno u formi matrice
	 * @param input broj kolone u koju se želi ubaciti žeton
	 * @return True ako je žeton uspješno ubačen. False u suprotnom
	 */
	public static boolean getForInput(char[][] state, int input) {
		input--;
		if (input < 0 || input > 6 || state[0][input] != ' ') {
			return false;
		}
		for (int i = 1; i <= state.length; i++) {
			if (i == state.length || state[i][input] != ' ') {
				state[i - 1][input] = 'O';
				return true;
			}
		}
		return true;
	}
}
