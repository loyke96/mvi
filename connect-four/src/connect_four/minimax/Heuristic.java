package connect_four.minimax;

/**
 * @author Željko Lojić
 *
 */
public class Heuristic {
	/**
	 * @param state stanje na platformi predstavljeno u formi matrice
	 * @return Heuristička procjena stanja na platformi
	 */
	public static double calculate(char[][] state) {
		double result = 0.0;
		for (int i = 0; i < state.length; i++) {
			for (int j = 0; j < state[i].length; j++) {
				if (i + 3 < state.length) {
					char[] line = { state[i][j], state[i + 1][j], state[i + 2][j], state[i + 3][j] };
					result += goodnessFactor(line);
					result -= dangerFactor(line);
					if (j + 3 < state[i].length) {
						char[] line1 = { state[i][j], state[i + 1][j + 1], state[i + 2][j + 2], state[i + 3][j + 3] };
						result += goodnessFactor(line1);
						result -= dangerFactor(line1);
					}
					if (j - 3 >= 0) {
						char[] line2 = { state[i][j], state[i + 1][j - 1], state[i + 2][j - 2], state[i + 3][j - 3] };
						result += goodnessFactor(line2);
						result -= dangerFactor(line2);
					}
				}
				if (j + 3 < state[i].length) {
					char[] line = { state[i][j], state[i][j + 1], state[i][j + 2], state[i][j + 3] };
					result += goodnessFactor(line);
					result -= dangerFactor(line);
				}
			}
		}
		return result;
	}

	/**
	 * @param tokens niz od 4 tokena za koje se određuje faktor opasnosi
	 * @return Faktor opasnosti niza
	 */
	private static double dangerFactor(char[] tokens) {
		int opponentTokensCount = 0;
		int playerTokensCount = 0;
		for (char token : tokens) {
			if (token == 'X') {
				playerTokensCount++;
			} else if (token == 'O') {
				opponentTokensCount++;
			}
		}
		if (opponentTokensCount == 4) {
			return Double.POSITIVE_INFINITY;
		}
		if (playerTokensCount > 0) {
			return 0;
		} else {
			return 100.0 * opponentTokensCount / 3.0;
		}
	}

	/**
	 * @param tokens niz od 4 tokena za koje se određuje faktor dobrote
	 * @return Faktor dobrote niza
	 */
	private static double goodnessFactor(char[] tokens) {
		int opponentTokensCount = 0;
		int playerTokensCount = 0;
		for (char token : tokens) {
			if (token == 'X') {
				playerTokensCount++;
			} else if (token == 'O') {
				opponentTokensCount++;
			}
		}
		if (playerTokensCount == 0) {
			return 0.0;
		} else if (playerTokensCount == 4) {
			return Double.POSITIVE_INFINITY;
		} else if (opponentTokensCount > 0) {
			return 11.0;
		} else {
			return 50.0 * playerTokensCount * playerTokensCount / 9.0;
		}
	}
}
